package task1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Locale {
    private String locale;
    private String countryName;
    private String manFirstNameFile;
    private String manSecondNameFile;
    private String manThirdNameFile;
    private String womanFirstNameFile;
    private String womanSecondNameFile;
    private String womanThirdNameFile;
    private String citiesNamesFile;
    private String streetsNamesFile;

    public Locale(String locale,
                  String countryName,
                  String manFirstNameFile,
                  String manSecondNameFile,
                  String manThirdNameFile,
                  String womanFirstNameFile,
                  String womanSecondNameFile,
                  String womanThirdNameFile,
                  String citiesNamesFile,
                  String streetsNamesFile) {
        this.locale = locale;
        this.countryName = countryName;
        this.manFirstNameFile = manFirstNameFile;
        this.manSecondNameFile = womanSecondNameFile;
        this.manThirdNameFile = manThirdNameFile;
        this.womanFirstNameFile = womanFirstNameFile;
        this.womanSecondNameFile = womanSecondNameFile;
        this.womanThirdNameFile = womanThirdNameFile;
        this.citiesNamesFile = citiesNamesFile;
        this.streetsNamesFile = streetsNamesFile;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getLocaleName() {
        return locale;
    }

    public List<String> getManFirstNames() throws IOException{
        return readLinesFromFile(manFirstNameFile);
    }

    public List<String> getManSecondNames() throws IOException{
        return readLinesFromFile(manSecondNameFile);
    }

    public List<String> getManThirdNames() throws IOException{
        return readLinesFromFile(manThirdNameFile);
    }

    public List<String> getWomanFirstNames() throws IOException{
        return readLinesFromFile(womanFirstNameFile);
    }

    public List<String> getWomanSecondNames()throws IOException {
        return readLinesFromFile(womanSecondNameFile);
    }

    public List<String> getWomanThirdNames() throws IOException{
        return readLinesFromFile(womanThirdNameFile);
    }

    public List<String> getCitiesNames() throws IOException{
        return readLinesFromFile(citiesNamesFile);
    }

    public List<String> getStreetsNames() throws IOException{
        return readLinesFromFile(streetsNamesFile);
    }

    private List<String> readLinesFromFile(String filePath) throws IOException{
        return Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
    }

    boolean checkLocale(String locale) {
        return getLocaleName().equals(locale.toLowerCase());
    }
}
