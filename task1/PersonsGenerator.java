package task1;

import java.io.IOException;
import java.util.*;

public class PersonsGenerator {
    private Locale[] locales;
    private static final int MAX_STREET_NUMBER = 100;
    private static final int COUNT_OF_MISTAKES_TYPES = 3;
    private static final int COUNT_OF_DEFAULT_LOCALES = 3;
    private static final int COUNT_OF_PERSON_PARAMS = 7;

    public PersonsGenerator() {
        locales = new Locale[COUNT_OF_DEFAULT_LOCALES];
        locales[0] = new Locale(
                "be_by",
                "Беларусь",
                "task1/names/bu_man_surnames",
        "task1/names/bu_man_names",
        "task1/names/bu_man_father_names",
        "task1/names/bu_woman_surnames",
        "task1/names/bu_woman_names",
        "task1/names/bu_woman_father_names",
        "task1/addresses/bu_cities",
        "task1/addresses/bu_streets"
        );
        locales[1] = new Locale(
        "ru_ru",
        "Российская Федерация",
        "task1/names/ru_man_surnames",
        "task1/names/ru_man_names",
        "task1/names/ru_man_father_names",
        "task1/names/ru_woman_surnames",
        "task1/names/ru_woman_names",
        "task1/names/ru_woman_father_names",
        "task1/addresses/ru_cities",
        "task1/addresses/ru_streets"
        );
        locales[2] = new Locale(
        "en_us",
        "United States of America",
        "task1/names/en_surnames",
        "task1/names/en_man_names",
        "task1/names/en_man_names",
        "task1/names/en_surnames",
        "task1/names/en_woman_names",
        "task1/names/en_woman_names",
        "task1/addresses/us_cities",
        "task1/addresses/us_streets"
        );
    }

    public void main(String[] argv) {
        if (checkParameters(argv)) {
            try {
                String[] personsWithMistakes = generatePersonsWithMistakes(
                        getLocale(argv[0]),
                        Integer.parseInt(argv[1]),
                        Double.parseDouble(argv[2])
                );

                for (String person: personsWithMistakes) {
                    System.out.println(person);
                }
            }

            catch (Exception e) {
                System.out.println("Something wend wrong!");
            }
        }
    }

    private String[] generatePersonsWithMistakes(Locale locale, int countOfPersons, double mistakesCount) throws IOException {
        Random random = new Random();

        String[][] generatedPersonList = generatePersonsList(
                countOfPersons,
                locale.getCountryName(),
                locale.getManFirstNames(),
                locale.getManSecondNames(),
                locale.getManThirdNames(),
                locale.getWomanFirstNames(),
                locale.getWomanSecondNames(),
                locale.getWomanThirdNames(),
                locale.getCitiesNames(),
                locale.getStreetsNames(),
                random
        );

        String[][] generatedPersonListWithMistakes = makeMistakesInPersonsParams(
                mistakesCount,
                generatedPersonList,
                random
        );

        return convertPersonsParamsToCSStrings(generatedPersonListWithMistakes);
    }



    private static String[][] generatePersonsList(int countOfPersons,
                                                 String country,
                                                 List<String> manFirstName,
                                                 List<String> manSecondName,
                                                 List<String> manThirdName,
                                                  List<String> womanFirstName,
                                                  List<String> womanSecondName,
                                                  List<String> womanThirdName,
                                                 List<String> cities,
                                                 List<String> streets,
                                                 Random random) {
        String[][] personsList = new String[countOfPersons][COUNT_OF_PERSON_PARAMS];
        int countOfManFirstNames = manFirstName.size();
        int countOfManSecondNames = manSecondName.size();
        int countOfManThirdNames = manThirdName.size();
        int countOfWomanFirstNames = womanFirstName.size();
        int countOfWomanSecondNames = womanSecondName.size();
        int countOfWomanThirdNames = womanThirdName.size();
        int countOfCities = cities.size();
        int countOfStreets = streets.size();
        boolean isMan;

        for(int i = 0; i < countOfPersons; i++) {
            isMan = random.nextBoolean();
            if(isMan) {
                personsList[i][0] = manFirstName.get(random.nextInt(countOfManFirstNames));
                personsList[i][1] = manSecondName.get(random.nextInt(countOfManSecondNames));
                personsList[i][2] = manThirdName.get(random.nextInt(countOfManThirdNames));

            }
            else {
                personsList[i][0] = womanFirstName.get(random.nextInt(countOfWomanFirstNames));
                personsList[i][1] = womanSecondName.get(random.nextInt(countOfWomanSecondNames));
                personsList[i][2] = womanThirdName.get(random.nextInt(countOfWomanThirdNames));
            }

            personsList[i][3] = country;
            personsList[i][4] = cities.get(random.nextInt(countOfCities));
            personsList[i][5] = streets.get(random.nextInt(countOfStreets));
            personsList[i][6] = "" + random.nextInt(MAX_STREET_NUMBER);
        }

        return personsList;
    }

    private String[][] makeMistakesInPersonsParams(double mistakeCount, String[][] personsParams, Random random) {
        String[][] personParamsWithMistakes = new String[personsParams.length][];

        for(int i = 0; i < personsParams.length; i++) {
            personParamsWithMistakes[i] = makeMistakesInPersonParams(mistakeCount, personsParams[i], random);
        }

        return personParamsWithMistakes;
    }

    private String[] makeMistakesInPersonParams(double mistakeCoefficient, String[] personParams, Random random) {
        int countOfMistake = (int) mistakeCoefficient;
        if (Math.random() < (mistakeCoefficient - countOfMistake)) {
            countOfMistake++;
        }

        ArrayList<LinkedList<Character>> listOfCharacterLists = new ArrayList<>();
        for (String param: personParams) {
            LinkedList<Character> paramCharacterList = new LinkedList<>();

            if (param.length() > 0) {
                for (char character : param.toCharArray()) {
                    paramCharacterList.add(character);
                }
            }

            listOfCharacterLists.add(paramCharacterList);
        }

        for(int i = 0; i < countOfMistake; i++) {
            int paramToChange = random.nextInt(COUNT_OF_PERSON_PARAMS);
            switch (random.nextInt(COUNT_OF_MISTAKES_TYPES)) {
                case 0:
                    makeAddMistake(listOfCharacterLists.get(paramToChange), random);
                    break;
                case 1:
                    makeRemoveMistake(listOfCharacterLists.get(paramToChange), random);
                    break;
                case 2:
                    makeChangeMistake(listOfCharacterLists.get(paramToChange), random);
                    break;
            }
        }

        String[] result = new String[personParams.length];
        for (int i = 0; i < personParams.length; i++) {
            result[i] = "";
            for (Object object: listOfCharacterLists.get(i).toArray()) {
                result[i] += (char)object;
            }
        }

        return result;
    }


    private static void makeAddMistake(LinkedList<Character> param, Random random) {
        int positionToPlace = 0;
        int paramSize = param.size();

        if (paramSize > 0) {
            positionToPlace = random.nextInt(paramSize);
        }

        param.add(positionToPlace, (char)random.nextInt(255));
    }

    private static void makeRemoveMistake(LinkedList<Character> param, Random random) {
        int paramSize = param.size();
        if (paramSize > 0) {
            param.remove(random.nextInt(paramSize));
        }
    }

    private static void makeChangeMistake(LinkedList<Character> param, Random random) {
        int paramSize = param.size();
        if (paramSize > 1) {
            int characterToChangeNumber = random.nextInt(paramSize - 1);
            Character characterToChange = param.remove(characterToChangeNumber);
            param.add(characterToChangeNumber + 1, characterToChange);
        }
    }

    private String[] convertPersonsParamsToCSStrings(String[][] personsParams) {
        int personsCount = personsParams.length;
        String [] result = new String[personsCount];

        for(int i = 0; i < personsCount; i++) {
            StringBuilder builder = new StringBuilder();

            for(int j = 0; j < COUNT_OF_PERSON_PARAMS - 1; j++){
                builder.append(personsParams[i][j]);
                builder.append(", ");
            }

            builder.append(personsParams[i][COUNT_OF_PERSON_PARAMS-1]);
            builder.append(";");
            result[i] = builder.toString();

        }

        return result;
    }


    private boolean checkParameters(String[] args) {
        if (args.length < 3) {
            System.out.println("Too few parameters(" + args.length
                    + "): 1) locale; 2) count of lines; 3) errors coefficient!");
            return false;
        }

        if (!checkLocale(args[0])) {
            System.out.println("Wrong locale!");
            return false;
        }

        try {
            if (Long.parseLong(args[1]) < 0) {
                System.out.println("Count of lines can't be negative!");
                return false;
            }

        }
        catch (NumberFormatException e) {
            System.out.println("Count of lines mast be natural number!");
            return false;
        }

        try {
            if (Double.parseDouble(args[2]) < 0) {
                System.out.println("Errors coefficient can't be negative!");
                return false;
            }
        }
        catch (NumberFormatException e) {
            System.out.println("Errors coefficient mast be real number!");
            return false;
        }


        return true;
    }

    private boolean checkLocale(String localeName) {
        for (Locale locale: locales) {
            if (locale.checkLocale(localeName)) {
                return true;
            }
        }
        return false;
    }

    private Locale getLocale(String localeName) throws RuntimeException {
        for(Locale locale: locales) {
            if (locale.checkLocale(localeName)) {
                return locale;
            }
        }

        throw new RuntimeException("No such locale");
    }



}
